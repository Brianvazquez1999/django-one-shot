from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm,TodoItemForm
# Create your views here.
def todo_list(request):
    list_items = TodoList.objects.all()
    context = {
        "list_items": list_items,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request,id):
    list_details = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": list_details
    }
    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            new_todo = form.save()
            return redirect("todo_list_detail", id=new_todo.id)
    else:
        form = TodoForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)

def edit_list(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form = TodoForm(instance=post)
    context = {
        "form":form
    }
    return render(request, "todos/edit.html", context)

def delete_todo(request, id):
    new = TodoList.objects.get(id=id)
    if request.method == "POST":
        new.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form":form
    }

    return render(request, "todos/createitem.html",context)

def edit_item(request, id):
    post = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form": form
    }
    return render(request, "todos/edititem.html", context)
