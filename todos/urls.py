from django.urls import path
from .views import todo_list, todo_list_detail, create_list, edit_list, delete_todo, create_item, edit_item
urlpatterns = [
path("items/<int:id>/edit/", edit_item, name="todo_item_update"),
path("items/create/", create_item, name="todo_item_create"),
path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
path("<int:id>/edit/", edit_list, name="todo_list_update"),
path("create/", create_list, name="todo_list_create"),
path("<int:id>/", todo_list_detail, name="todo_list_detail"),
path("", todo_list, name="todo_list_list"),
]
